import java.util.Vector;

class DemoVector
{
	public static void main (String[] args) 
	{
		//demo1();
		demo2();
	}
	
	private static void demo1()
	{
		Vector<String> v = new Vector<String>(); // c'est l'�quivalent de String[] tab = new String[TAILLE];
		
		v.add("Premier pas"); // tab[i] = ... devient v.add(...)
		v.add("deuxi�me");
		v.add("troisi�me");
		v.add("quatri�me");
		
		// v.add(78); // refus� par le compilateur parce que Vector<String>
		//v.add(new Object()); // refus� par le compilateur parce que Vector<String>
		
		
		System.out.println (v);
		
		/* tab.length devient v.size() */
		for (int i = 0; i < v.size(); i++) {
			/* tab[i] devient v.get(i) */
			System.out.println (i+" - "+v.get(i));
		}
			
		/* attention � la diff�rence entre 
		   Vector<Integer> h; //OK !
		    et 
		   Vector<int> h; // ne compile pas
		*/
	}
	
	private static void demo2()
	{
		Vector<Integer> liste = new Vector<Integer>();// par d�faut : capacit� initiale = 10 et double ensuite 
		
		
		//Vector<Integer> liste = new Vector<Integer>(5);// capacit� initiale = 5 , par d�faut : double ensuite
		//Vector<Integer> liste = new Vector<Integer>(9, 7);// capacit� initiale = 9 , augmente de 7 ensuite
		
		System.out.println ("capacit� initiale : " + liste.capacity());
		
		int limite = liste.capacity();
		for (int i = 0; i <= limite; i++) {
			liste.add(i);
		}
			
		System.out.println ("nouvelle capacit�  : " + liste.capacity());
		
		limite = liste.capacity();// var limite d�j� d�clar�e => pas de int limite
		for (int i = 0; i <= limite; i++) {
			liste.add(i);
		}
			
		System.out.println ("au final capacit�  : " + liste.capacity());
			
	}
	
	/**
	 * Dans un vecteur, comme pour les tableaux, on peut mettre des objets!
	 */
	private static void question1() {
		Vector<Herisson> v = new Vector<Herisson>();
		
		v.add(new Herisson("Sonic", 3));
		v.add(new Herisson("Bob l'�ponge", 1));
	}
	
	static class Herisson {
		/* private */ String nom;
		/* private */ int nbPommesMangees;
		
		Herisson(String pNom, int pNbPommes) {
			nom = pNom;
			nbPommesMangees = pNbPommes;
		}
	}
	

}