

class DemoExosSerie1
{
	public static void main (String[] args) 
	{
		exo1();
		exo4();

	}
	
	static void exo1()
	{
		double rayon = 10;
		if ( rayon < 0 )
			System.out.println ("Impossible !");
		else
		{
			System.out.println ("rayon = "+rayon);
			System.out.println ("surface = "+Math.PI*rayon*rayon);
		}		
	}
	
	static void exo4() {
		int p = 3;
		int n = 18;
		System.out.println ("Nombres pairs entre " + p + " et " + n);
		for(int i = p; i <= n; i++) {
			if((i % 2) == 0) {
				System.out.println (i);
			}
		}
	}
	

}