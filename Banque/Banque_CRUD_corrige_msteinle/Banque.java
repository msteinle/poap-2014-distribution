import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Vector;

/**
 * Corrigé pour les exercices "CRUD" (create read update delete) du groupe 1.
 * @author msteinle
 */
public class Banque
{
    private String nomBanque;
    private Vector<Compte> lesComptes;

    public Banque(String nomBanque, String nomFichier)
    {
        this.nomBanque = nomBanque;
        lireComptes(nomFichier);

    }

    private void lireComptes(String nomFichier)
    {
        lesComptes = new Vector<Compte>();

        try
        {
            Scanner fluxFichier = new Scanner(new File(nomFichier));
            while (fluxFichier.hasNext())
            {
                lesComptes.add(traiterLigne(fluxFichier.nextLine()));
            }
        } catch (IOException ioe)
        {
            System.err.println(ioe);
        }
    }

    private static Compte traiterLigne(String ligne)
    {
        Scanner fluxLigne = new Scanner(ligne);
        return new Compte(fluxLigne.next(), fluxLigne.nextDouble(), fluxLigne.nextDouble(), fluxLigne.nextBoolean(), fluxLigne.nextInt());
    }

    int getNombreComptes()
    {
        return lesComptes.size();
    }

    int trouverPosition(String noCompte)
    {
        for (int i = 0; i < lesComptes.size(); i++)
        {
            if (lesComptes.get(i).noCpt.equals(noCompte))
            {
                return i;
            }
        }
        return -1;
    }


    /**
     * @return Le compte avec le numéro donné, null sinon.
     */
    Compte trouverCompte(String noCompte)
    {
        for (Compte c : lesComptes)
        {
            if (c.noCpt.equals(noCompte))
            {
                return c;
            }
        }
        return null;
    }


    public void afficherComptes()
    {
        for (Compte c : lesComptes)
        {
            System.out.println(c);
        }
    }

    public void afficherCompte(String noCompte)
    {
        System.out.println(trouverCompte(noCompte));
    }

    /**
     * Ajoute un compte à la fin de la liste des comptes, si le numďéro de compte n'existe pas encore.
     *
     * @return true si le compte a été ajouté, false sinon (numéro de compte existe déjà).
     */
    public boolean ajouterCompte(Compte nouveauCompte)
    {
        if (trouverCompte(nouveauCompte.noCpt) != null)
        { // un compte du même numéro existe déjà
            return false;
        }
        else // ce else n'est pas obligatoire, à cause du return ci-dessus (voir méthode insererCompte)
        {
            return lesComptes.add(nouveauCompte);
        }
    }

    /**
     * Insère un compte à la position spécifié, en décalant les suivants.
     *
     * @return true si le compte a été ajouté, false sinon (numéro de compte existe déjà).
     */
    public boolean insererCompte(int position, Compte nouveauCompte)
    {
        if (trouverCompte(nouveauCompte.noCpt) != null)
        {
            return false;
        }
        lesComptes.add(position, nouveauCompte);
        return true;
    }

    /**
     * Supprime le compte passé en paramètre.
     *
     * @return true si la suppression a réussi, false sinon (compte pas trouvé).
     */
    public boolean supprimerCompte(Compte compte)
    {
        return lesComptes.remove(compte);
    }

    /**
     * Supprime le compte à la position spécifiée.
     *
     * @return true si la suppression a réussi, false sinon (compte pas trouvé).
     */
    public boolean supprimerCompte(int position)
    {
        if(position < 0 || position >= lesComptes.size()) {
            System.out.println("Index invalide: " + position + " (taille liste: " + lesComptes.size() + ")");
            return false;
        }
        lesComptes.remove(position);
        return true;
    }

    public boolean supprimerCompte(String noCompte)
    {
        return lesComptes.remove(trouverCompte(noCompte));
    }

    // TODO 5: Comment faire pour modifier un compte de la banque depuis la classe TestBanque?

    // Dans Banque, nous pourrions faire le suivant:
    public boolean modifierTypeCompte(String noCompte, int nouveauType)
    {
        Compte c = trouverCompte(noCompte);
        if(c == null)
        {
            return false;
        }
        else
        {
            c.typeCpt = nouveauType;
            return true;
        }
    }

    public String toString()
    {
        return nomBanque + "< #comptes= " + lesComptes.size() + ">";
    }


}