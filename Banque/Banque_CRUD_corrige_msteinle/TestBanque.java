/**
 * Corrigé pour les exercices "CRUD" (create read update delete) du groupe 1.
 *
 * @author msteinle
 */
public class TestBanque
{
    public static void main(String[] args)
    {
        Banque banque = new Banque("ThePirateBank", "ListeCompte.txt");
        testAjouter_ok(banque);
        testAjouter_ko(banque);
//        testInserer_ko(banque);
//        testInserer_debut_ok(banque);
//        testInserer_milieu_ok(banque);
//        testInserer_fin_ok(banque);
        testSupprimer_parInstance_ok(banque);
        testSupprimer_parInstance_ko(banque);
        testSupprimer_parPosition_ok(banque);
        testSupprimer_parPosition_ko(banque);
        testSupprimer_parNumero_ok(banque);
        testSupprimer_parNumero_ko(banque);
        testModifierCompte(banque);

    }

    private static void testModifierCompte(Banque banque)
    {
        System.out.println("== Test testModifierCompte ==");
        String noCompte = "QL622";
        Compte c = banque.trouverCompte(noCompte);
        System.out.println("Le compte avant: " + c);

        c.typeCpt = Compte.COMPTE_COURANT;

        System.out.println("Le compte après: " + banque.trouverCompte(noCompte));
    }


    private static void testSupprimer_parPosition_ok(Banque b)
    {
        System.out.println("== Test supprimer par Position OK ==");
        int index = 1;

        System.out.println("Le compte avant (regarder 2ème ligne): ");
        b.afficherComptes();

        System.out.println("Le résultat (true attendu): " +
                b.supprimerCompte(1));

        System.out.println("Le compte après (regarder 2ème ligne, doit avoir disparu): ");
        b.afficherComptes();

        System.out.println();
    }

    private static void testSupprimer_parPosition_ko(Banque b)
    {
        System.out.println("== Test supprimer par position KO ==");
        System.out.println("La banque avant:" + System.lineSeparator() + b);

        System.out.println("Le résultat (false attendu): " +
                b.supprimerCompte(-1));
        System.out.println("Le résultat (false attendu): " +
                b.supprimerCompte(b.getNombreComptes()));

        System.out.println("La banque avant (attendu nombre comptes inchangé)" + System.lineSeparator() + b);

        System.out.println();
    }

    private static void testSupprimer_parInstance_ok(Banque b)
    {
        System.out.println("== Test supprimer par instance OK ==");
        String noCompte = "NM557";

        System.out.print("Le compte avant (compte attendu): ");
        b.afficherCompte(noCompte);

        Compte c = b.trouverCompte(noCompte);
        System.out.println("Le résultat (true attendu): " +
                b.supprimerCompte(c));

        System.out.print("Le compte après (null attendu): ");
        b.afficherCompte(noCompte);

        System.out.println();
    }

    private static void testSupprimer_parInstance_ko(Banque b)
    {
        System.out.println("== Test supprimer par instance KO ==");
        String noCompte = "NM557";

        System.out.print("Le compte avant (compte attendu): ");
        b.afficherCompte(noCompte);

        Compte c = new Compte(noCompte, 222.92, 0, true, 1);
        System.out.println("Le résultat (false attendu): " +
                b.supprimerCompte(c));

        System.out.print("Le compte après (compte attendu): ");
        b.afficherCompte(noCompte);

        System.out.println();
    }


    private static void testSupprimer_parNumero_ok(Banque b)
    {
        System.out.println("== Test supprimer par numéro OK ==");
        String noCompte = "VE171";

        System.out.print("Le compte avant (compte attendu): ");
        b.afficherCompte(noCompte);

        System.out.println("Le résultat (true attendu): " +
                b.supprimerCompte(noCompte));

        System.out.print("Le compte après (null attendu): ");
        b.afficherCompte(noCompte);

        System.out.println();
    }

    private static void testSupprimer_parNumero_ko(Banque b)
    {
        System.out.println("== Test supprimer par numéro KO ==");
        String noCompte = "bob2334";

        System.out.print("Le compte avant (null attendu): ");
        b.afficherCompte(noCompte);

        System.out.println("Le résultat (false attendu): " +
                b.supprimerCompte(noCompte));

        System.out.print("Le compte après (null attendu): ");
        b.afficherCompte(noCompte);

        System.out.println();
    }

    private static void testAjouter_ok(Banque b)
    {
        System.out.println("== Test ajouter OK ==");
        String noCompte = "schtroumpf";

        System.out.print("Le compte avant ajout (null attendu): ");
        b.afficherCompte(noCompte);

        System.out.println("Le résultat (true attendu): " +
                b.ajouterCompte(new Compte(noCompte, 1000, 100, true, Compte.COMPTE_COURANT)));

        System.out.println("Le compte après ajout (compte " + noCompte + " attendu à la fin): ");
        b.afficherComptes();

        System.out.println();
    }

    private static void testAjouter_ko(Banque banque)
    {
        System.out.println("== Test ajouter KO ==");
        String noCompte = "NM557"; // compte existant

        System.out.print("Le compte avant ajout (compte attendu): ");
        banque.afficherCompte(noCompte);

        boolean resultat = banque.ajouterCompte(new Compte(noCompte, 999999, 100, true, Compte.COMPTE_COURANT));
        System.out.println("Le résultat (false attendu): " +
                resultat);

        System.out.print("Le compte après ajout (compte attendu, avec solde <> 999999): ");
        banque.afficherCompte(noCompte);

        System.out.println();
    }


}