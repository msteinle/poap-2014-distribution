/****************************/
/* CARVAS MARTINS BRUNO    */
/****************************/

 
class Compte
{
	/* les 3 constantes pour les 3 types de compte */
    public static final int COMPTE_EPARGNE = 1, COMPTE_COURANT = 2, COMPTE_PREVOYANCE = 3;

    /* private */ String noCpt;
    /* private */ double soldeCpt;
    /* private */ double limiteCreditCpt;
    /* private */ boolean actifCpt;
    /* private */ int typeCpt;


    public Compte(String numeroCompte, double soldeCompte, double limiteCreditCompte, boolean actifCompte, int typeCompte)
    {
        noCpt = numeroCompte;
        soldeCpt = soldeCompte;
        limiteCreditCpt = limiteCreditCompte;
        actifCpt = actifCompte;
        typeCpt = typeCompte; 
    }

    public boolean crediter(double montant)
    {
        if (montant < 0)
        {
            return false;
        }
        else
        {
	        soldeCpt = soldeCpt + montant;
	        return true;
        }
    }

    public boolean debiter(double montant)
    {
        if (typeCpt == COMPTE_PREVOYANCE)
        {
            return false;
        }
        else if (montant < 0)
        {
            return false;
        }
        else if (soldeCpt - montant < -limiteCreditCpt)
        {
            return false;
        }
        else
        {
	        soldeCpt = soldeCpt - montant;
	        return true;
        }
    }

	public boolean solderPrevoyance(Compte CompteDestination)
	{ 
		if (typeCpt==COMPTE_PREVOYANCE)
		{
			if (CompteDestination.typeCpt==COMPTE_COURANT)
			{
				CompteDestination.crediter(soldeCpt);
				soldeCpt = 0;
				actifCpt = false;
				System.out.println ("compte sold�!");
				return true;
			}
			else
			{
				System.out.println ("ce n'est pas un compte courant, le compte ne peut pas �tre cr�dit�!");
				return false;
			}
			
		}	
		else
		{
			System.out.println ("ne pas solder le compte!");
			return false;
		}
		
	}

    public String toString()
    {
    	String affichageCommunPremier = "Compte ";
    	String affichageCommunDeuxieme = "< num. '" + noCpt; 
    	String affichageCommunTroisieme = "', solde = " + soldeCpt +" >";
    	String affichageInactif = " (INACTIF), ";
    	
    	if (typeCpt == COMPTE_EPARGNE)
    	{
    		affichageCommunPremier = affichageCommunPremier + "Epargne ";
    	}
    	else if (typeCpt == COMPTE_COURANT)
    	{
    		affichageCommunPremier = affichageCommunPremier + "Courant ";
    	}
    	else
    	{
    		affichageCommunPremier = affichageCommunPremier + "Pr�voyance ";
    	}
        
        if (actifCpt==false)
        {
        	String comptesInactifs = affichageCommunPremier + affichageInactif + affichageCommunDeuxieme + affichageCommunTroisieme;
        	return comptesInactifs;
        }
        else
        {
        	String comptesActifs = affichageCommunPremier + affichageCommunDeuxieme + affichageCommunTroisieme;
        	return comptesActifs;
        }
    }
    
}
