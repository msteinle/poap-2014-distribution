/**
 * @(#)TestBanque.java
 *
 *
 * @author 
 * @version 1.00 2015/4/23
 */

//Les imports n�cessaires au fonctionnement
import java.util.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.lang.Exception;
import java.lang.String;
import java.lang.System;
import java.util.Scanner;
import java.io.File;

//Classe TestBanque
public class TestBanque 
{
	//M�thode main - Ici, on appel les m�thodes Public
    public static void main (String[] args) 
    {
    	//Cr�ation d'une nouvelle banque d'apr�s un fichier texte, Calcul de la somme de tous les comptes de cette banque et Affichage
    	Banque maBanque = new Banque("UBS", "listeCompteUBS.txt");
    	System.out.println ("UBS");
    	System.out.println ("---");
    	maBanque.afficherComptes();
    	double sommeUBS = maBanque.calculerTotalSoldeBanque();
    	System.out.println ("Le montant total de la banque s'�l�ve � " + sommeUBS + ".");
    	
    	System.out.println ();
    	
    	//Cr�ation d'une deuxi�me banque d'apr�s un fichier texte, Calcul de la somme de tous les comptes de cette banque et Affichage
    	Banque maDeuxiemeBanque = new Banque ("Cr�dit Suisse", "listeCompteCS.txt");
    	System.out.println ("Cr�dit Suisse");
    	System.out.println ("-------------");
    	maDeuxiemeBanque.afficherComptes();
    	double sommeCS = maDeuxiemeBanque.calculerTotalSoldeBanque();
    	System.out.println ("Le montant total de la banque s'�l�ve � " + sommeCS + ".");
 
 		System.out.println ();
 		System.out.println ();
 		
 		//Affichage du num�ro d'un compte de la premi�re banque mis en param�tre
 		System.out.println ("Compte UBS n� ");
 		System.out.println ("--------------");
 		maBanque.afficherCompte("NA759");
 		
 		System.out.println();
 		System.out.println ();
 		
 		//Affichage du retour TRUE ou FALSE pour le d�bit(+) d'un compte mis en param�tre de la premi�re banque
 		System.out.println ("D�p�t");
 		System.out.println ("-----");
 		maBanque.d�poser("DE582", 50);
 		
 		System.out.println ();
 		System.out.println ();
 		
 		//Affichage du retour TRUE ou FALSE pour le cr�dit(-) d'un compte mis en param�tre de la premi�re banque
 		System.out.println ("Retrait");
 		System.out.println ("-------");
 		maBanque.retirer("DE582", 50);
 		
 		System.out.println ();
 		System.out.println ();
 		
 		//Cr�ation du nouveau compte et affichage des comptes du vector pour la v�rification
 		Compte nouveauCompte = new Compte("POAP",500,1000,true,Compte.COMPTE_COURANT);
 		System.out.println ("Ajout d'un nouveau compte � la fin du vector de comptes");
 		
 		boolean resultat = maBanque.ajouterCompte(nouveauCompte);
 
 		if (resultat==true)
 		{
 			System.out.println ("Compte ajout� avec succ�s");
 		}	
 		else
 		{
 			System.out.println ("Impossible d'ajouter ce compte, compte existant");
 		}
 		
 		maBanque.afficherComptes();
 		
 		System.out.println ();
 		System.out.println ();
 		
 		//Cr�ation du nouveau compte � la position sp�cifi�e et affichage des comptes du vector pour la v�rification
 		testerInsererCompte(maBanque);
 		
 		
 		//Supprimer un compte du vector et affichage des comptes du vector pour la v�rification
 		System.out.println ("Suppression d'un compte du vector de comptes");
 		
 		maBanque.trouverCompte("HI519");
 		
 		Compte compte = new Compte("HI519",783.11, 0, true, 1);
 		
 		boolean resultat3 = maBanque.supprimerCompte(compte);
 
 		if (resultat3==true)
 		{
 			System.out.println ("Compte supprim� avec succ�s");
 		}	
 		else
 		{
 			System.out.println ("Impossible de supprimer ce compte, compte inexistant");
 		}
 		
 		maBanque.afficherComptes();
	}
	
	static void testerInsererCompte(Banque maBanque) {
		Compte nouveauCompte2 = new Compte("RUBEN",200,500,true,1);
 		System.out.println ("Ajout d'un nouveau compte � la position sp�cifi�e du vector de comptes");
 		
 		boolean resultat2 = maBanque.insererCompte(0,nouveauCompte2);
 
 		if (resultat2==true)
 		{
 			System.out.println ("Compte ajout� avec succ�s");
 		}	
 		else
 		{
 			System.out.println ("Impossible d'ajouter ce compte, compte existant");
 		}
 		
 		maBanque.afficherComptes();
 		
 		System.out.println ();
 		System.out.println ();
	}
    
}