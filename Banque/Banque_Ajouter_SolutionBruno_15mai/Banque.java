/**
 * @(#)Banque.java
 *
 *
 * @author Bruno Carvas Martins
 * @version 1.00 2015/4/23
 */

//Les imports n�cessaires
import java.util.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.lang.Exception;
import java.lang.String;
import java.lang.System;
import java.util.Scanner;
import java.io.File;


//Classe banque
public class Banque 
{
	private String nomBanque;
	private Vector<Compte> lesComptes;
	
	//Constructeur banque - Ici, on appelle les m�thodes Private
    public Banque(String nomBanque, String nomFichier) 
    {
    	lireComptes(nomFichier);
    	
    	if (nomFichier.equals("listeCompteUBS.txt"))
    	{
    	
    		//Affichage de l'index d'un compte du vector mis en param�tre
    		System.out.println ("Index");
    		System.out.println ("-----");
    		System.out.println ("L'index du compte mis en param�tre est ");
    		trouverIndexCompte("RJ275");
    	
    	}
    	
    	System.out.println ();
    }
    
    //Lire les comptes depuis un fichier et remplir un nouveau vector
    private void lireComptes(String nomFichier)
    {
    	Vector<Compte> resultat = new Vector<Compte>();
			
		try
		{
			Scanner fluxFichier = new Scanner(new File(nomFichier));
				
			int position = 0;
				
			while ( fluxFichier.hasNext() )
			{
				resultat.add(traiterLigne(fluxFichier.nextLine()));  

			}
			
			this.lesComptes = resultat;
		}
		catch (IOException ioe)
		{
			System.out.println (ioe);
		}
    }
    
    private Compte traiterLigne(String ligne)
	{
		Scanner fluxLigne = new Scanner(ligne);
		return new Compte(fluxLigne.next(), fluxLigne.nextDouble(), fluxLigne.nextDouble(), 
				              fluxLigne.nextBoolean(), fluxLigne.nextInt());
	}
    
    
    //Afficher tous les comptes du vector
    public void afficherComptes()
    {
		for (int i = 0; i < lesComptes.size(); i++) 
		{
			System.out.println(lesComptes.get(i)); 
		}
		
		System.out.println ();
    }
    
    
    //Calculer le total du solde de la banque
    public double calculerTotalSoldeBanque()
    {
    	double somme = 0;
    	
    	for (int i = 0; i < lesComptes.size(); i++) 
		{ 
			somme = somme + lesComptes.get(i).soldeCpt; 
		}
		
		return somme;
    }
 	
 	
 	//Afficher un seul compte du vector mis en param�tre
 	public void afficherCompte(String noCompte)
 	{
 		for (int i = 0; i < lesComptes.size(); i++) 
		{
			if (lesComptes.get(i).noCpt.equalsIgnoreCase(noCompte))
			{
				 System.out.println(lesComptes.get(i).noCpt);
			}
			
		}
		
 	}
 	
 	
 	//D�poser un montant mis en param�tre sur un compte du vector mis aussi en param�tre
 	public boolean d�poser(String noCompte, double montant)
    {
    	for (int i = 0; i < lesComptes.size(); i++) 
		{
    	
        	if ((lesComptes.get(i).noCpt.equalsIgnoreCase(noCompte))&&(lesComptes.get(i).actifCpt==true)&&(montant > 0))
        	{
        		lesComptes.get(i).soldeCpt = lesComptes.get(i).soldeCpt + montant;
            	System.out.println ("D�p�t autoris� sur le compte n� ");
            	this.afficherCompte(noCompte);
            	return true;
        	}
        	
		}
		
        System.out.println ("D�p�t refus� sur le compte n� ");
        this.afficherCompte(noCompte);
        return false;
        
    }
      
        
    //Retirer un montant mis en param�tre sur un compte du vector mis aussi en param�tre    
    public boolean retirer(String noCompte, double montant)
    {
       	for (int i = 0; i < lesComptes.size(); i++) 
		{
    	
        	if (((lesComptes.get(i).noCpt.equalsIgnoreCase(noCompte))&&(lesComptes.get(i).soldeCpt - montant < -lesComptes.get(i).limiteCreditCpt)&&(lesComptes.get(i).actifCpt==true)))
        	{
        		lesComptes.get(i).soldeCpt = lesComptes.get(i).soldeCpt - montant;
        		System.out.println ("Retrait autoris� sur le compte n� ");  // ou + lesComptes.get(i).noCpt);
        		this.afficherCompte(noCompte);
        		return true;
        	}
      
		}
		
		System.out.println ("Retrait refus� sur le compte n� ");
        this.afficherCompte(noCompte);
        return false;
		
    }
 	
 	
 	//Trouver l'index d'un compte du vector mis en param�tre
 	private int trouverIndexCompte(String noCompte)
 	{
 		int i;
 		
 		for (i = 0; i < lesComptes.size(); i++) 
		{
			if (lesComptes.get(i).noCpt.equalsIgnoreCase(noCompte))
			{
				System.out.println (i);
			}
			
		}
		
		return i;
 	}
 	
 	
 	//Trouver la position avec un seul return FOR
	 int trouverPosition(String noCompte){
		int i = 0;
		for (i = 0; i<lesComptes.size() && !lesComptes.get(i).noCpt.equals(noCompte) ; i++){
			// rien � faire ici...
		}
		if(lesComptes.size() == i) {
			i = -1;
		}
		return i;
	}
	
	
	//Trouver un compte avec le num�ro du compte en param�tre, return le compte avec le num�ro donn�, sinon NULL
	Compte trouverCompte(String noCompte){
		
		for (int i = 0; i<lesComptes.size(); i++)
		{
			if (lesComptes.get(i).noCpt.equals(noCompte)) 
			{
				return lesComptes.get(i);
			}
		}
		return null;	
	}
 	
 	
 	//Ajouter un compte � la fin de la liste des comptes du vector
 	public boolean ajouterCompte(Compte nouveauCompte) 
 	{
		if (trouverCompte(nouveauCompte.noCpt)!=null)
		{
			return false;
		}
		lesComptes.add(nouveauCompte);
		return true;
 	}
 	
 	
 	//Ins�rer un compte � la position sp�cifi�e
	public boolean insererCompte(int position, Compte nouveauCompte) 
	{
		if (trouverCompte(nouveauCompte.noCpt)!=null)
		{
			return false;
		}
		lesComptes.insertElementAt(nouveauCompte,position);
		return true;
	}
	
	
	// Supprimer un compte pass� en param�tre
	//@return true si la suppression a r�ussi, false sinon (compte pas trouv�)
	public boolean supprimerCompte(Compte compte) 
	{
		
		for (int i = 0; i < lesComptes.size(); i++) 
		{
			if (lesComptes.get(i).noCpt.equals(compte.noCpt))
			{
				lesComptes.removeElement(compte); 
				return true; //return lesComptes.removeElement(compte);
			}
			
		}
		return false;
	}
	
	 //Supprimer le compte � la position sp�cifi�e.
	 //@return true si la suppression a r�ussi, false sinon (compte pas trouv�)
	 
	/*public boolean supprimerCompte(int position) 
	{
		
	}
	
	// TODO 5: Comment faire pour modifier un compte de la banque depuis la classe TestBanque?
	
	public String toString()
	{

        return nomBanque; 
    } toString*/
 		
}