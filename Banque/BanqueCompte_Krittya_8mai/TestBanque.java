/******************************/
/* NOIPHENGK  */
/******************************/
import java.util.*;
import java.io.*;

public class TestBanque 
{

    public static void main(String[] args){
        /**** Banque num�ro 1 ****/
      
        Banque laBanque = new Banque("UBS", "ListeCompte.txt");
        
        System.out.println ("Notre banque : " + laBanque + System.lineSeparator());
        
        laBanque.afficherComptes();	
        	
        	
        /**** Banque num�ro 2 ****/
         
        Banque laBanque2 = new Banque("BobEtSesAmis", "ListeCompte2.txt");
        
        System.out.println (System.lineSeparator() + "Notre banque : " + laBanque2 + System.lineSeparator());
        
        laBanque2.afficherComptes();
        
        System.out.println ("Le total des soldes : " + laBanque2.nosTotaux());
        
        /**** Afficher UN compte ****/
        System.out.println (System.lineSeparator() + "Le compte � afficher :");
        laBanque.afficherCompte("NM557");
        
        
        System.out.println (System.lineSeparator() + "Index comptes existants :");
        System.out.println (laBanque.trouverPosition4("IL854"));
        System.out.println (laBanque.trouverPosition4("NM557"));
        System.out.println (laBanque.trouverPosition4("VE171"));
          
        
        System.out.println (System.lineSeparator() + "Index compte inexistant :");
        System.out.println (laBanque.trouverPosition4("dsfdxsffd"));
        
        /**** Deposer de la thune mecc sur un compte ****/
        System.out.println (System.lineSeparator() + "Je souhaite d�poser 10.00 sur le compte IL854:");
       	String noCmpt = "IL854";
       	laBanque2.afficherCompte(noCmpt);
       	if(laBanque2.deposer(noCmpt, 10)){
       		laBanque2.afficherCompte(noCmpt);
       	}
   
		/**** Retirer de la thune mecc sur un compte ****/
        System.out.println (System.lineSeparator() + "Je souhaite retiser 10.00 sur le compte MJ481:");
       	if(laBanque2.retirer("MJ481", 10)){
       		laBanque2.afficherCompte("MJ481");
       	}
    }//fin main
    
    
}//fin public class
