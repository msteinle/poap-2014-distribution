/******************************/
/* NOIPHENGK  */
/******************************/
import java.util.*;
import java.io.*;

public class Banque 
{
	private String nomBanque;
	private Vector<Compte> lesComptes;
	
    public Banque(String pNomBanque, String pNomFichier){	
    	nomBanque = pNomBanque;
    	lireComptes(pNomFichier);

    }//Banque

    private void lireComptes(String nomFichier){
		Vector<Compte> resultat = new Vector<Compte>();
			
		try{
			Scanner fluxFichier = new Scanner(new File(nomFichier));
				
			while ( fluxFichier.hasNext() ){
				resultat.add(traiterLigne(fluxFichier.nextLine()));
			}
			lesComptes = resultat;
		}
		catch (IOException ioe){
			System.out.println (ioe);
		}
	}//lireComptes
	
	private static Compte traiterLigne(String ligne){
		Scanner fluxLigne = new Scanner(ligne);
		return new Compte(fluxLigne.next(), fluxLigne.nextDouble(), fluxLigne.nextDouble(), fluxLigne.nextBoolean(), fluxLigne.nextInt());
	}//traiterLigne

	private int trouverPosition(String noCompte){
		for (int i = 0; i<lesComptes.size(); i++){
			if(lesComptes.get(i).noCpt.equals(noCompte)){
				return i;
			}
		}
		return -1;
	}//trouverPosition
	
	private int trouverPosition2(String noCompte){
		int i = 0;
		boolean trouve = false;
		
		while(i <lesComptes.size() && trouve == false){
			if(lesComptes.get(i).noCpt.equals(noCompte)){
				trouve = true;
			}else{
				i++;
			}
		}
		
		if(trouve){
			return i;
		}
		else{
			return -1;
		}
		
		//return trouve? i : -1;
		
	}//trouverPosition2	
	
	private int trouverPosition3(String noCompte){
		int i = 0;
		int index = -1;
		
		while(i <lesComptes.size() && -1 == index){
			if(lesComptes.get(i).noCpt.equals(noCompte)){
				index=i;		
			}else{
				i++;
			}
		}
		return index;		

	}//trouverPosition3		
	
	/**
	 * Une version avec "for" et un seul "return"
	 */
	 int trouverPosition4(String noCompte){
		int i = 0;
		for (i = 0; i<lesComptes.size() && !lesComptes.get(i).noCpt.equals(noCompte) ; i++){
			// rien � faire ici...
		}
		if(lesComptes.size() == i) {
			i = -1;
		}
		return i;
		
	}//trouverPosition
	
	/**
	 *
	 * @return Le compte avec le num�ro donn�, null sinon.
	 **/
	Compte trouverCompte(String noCompte){
	
		return null;	
	}
	
    public double nosTotaux(){
    	double somme = 0;
    
    	for (int i = 0; i<lesComptes.size(); i++){
    		somme = somme + lesComptes.get(i).soldeCpt;
    	}	
    	return somme;
    }//nosTotaux
    
    public boolean deposer(String noCompte, double montant){
		
		int indice = trouverPosition2(noCompte);
		
		if(indice!=-1){
    		return lesComptes.get(indice).crediter(montant);
    	}else{
			return false;
		}
		
    }//deposer
    
     public boolean retirer(String noCompte, double montant){
		
		int indice = trouverPosition(noCompte);
		
		if(indice!=-1){
    		 return lesComptes.get(indice).debiter(montant);
		}else{
			return false;
		}
		
    }//retirer
    
    public void afficherComptes(){
		for (int i = 0; i < this.lesComptes.size(); i++){
			System.out.println (this.lesComptes.get(i));
		}
	}//afficherComptes
	
	public void afficherCompte(String noCompte){
		int indice = trouverPosition3(noCompte);
		
		if (indice!=-1){
			System.out.println (lesComptes.get(indice));
		}
	}//afficherCompte
	
	public String toString(){

        return nomBanque;
    }//toString
    	

}//fin classe Banque