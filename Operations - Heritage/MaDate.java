public class MaDate
{
	int jourDate;
	int	moisDate;
	int anneeDate;
	
	Madate(int jour, int mois, int annee)
	{
		jourDate = jour;
		moisDate = mois;
		anneeDate = annee;
	}
	
	public String toString()
	{
		return jourDate+"/"+moisDate+"/"+anneeDate;
	}
	
}