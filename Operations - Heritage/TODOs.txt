Héritage en Java

Greenfoot

1. Ajoutez une sous-classe de Hedgehog HerissonAVentreBlanc à votre scénario
=> les hérissons à ventre blanc (http://fr.wikipedia.org/wiki/Atelerix_albiventris) ont un attribut supplémentaire "nombreCheveuxBlancs"

=> Sur la page: http://openclassrooms.com/courses/apprenez-a-programmer-en-java/l-heritage-1
Cherchez le mot clé "constructeur d'initialisation" sur cette page pour savoir comment créer le constructeur 


Opérations bancaires

1. Créer la classe Operation et ses sous-classes selon le diagramme de classe fourni
=> au lieu de la classe java.util.Date, utilisez la classe MaDate qui est fournie
=> utilisez le constructeur de la super-classe dans les sous-classes (via l'appel à "super")

2. Le mot clé "abstract", quel effet a-t-il? Pourquoi s'en servir?
=> regardez encore une fois l'exemple de Greenfoot. La classe Actor est abstraite. Quelle est sa particularité?

3. Testez toutes les classes que vous venez de créer depuis une classe de test
=> créez des méthodes de test individuelles que vous appelez depuis la méthode main de la classe de test.

4. Modifiez votre classe Banque pour ajouter la lecture d'une liste d'opérations selon le fichier fourni
=> Le Vector sera de type Operation, mais contiendra les sous-classes qui correspondent aux types indiqués dans le fichier

5. Créez une méthode dans la classe Banque qui retourne toutes les opérations qui concernent un numéro de compte donné: Vector<Operation> trouverOperations(String noCompte)
=> Il s'agit de retourner ces opérations, et non de les afficher!
=> Testez avec des méthodes de test appropriés.

6. Créez une méthode dans la classe Banque qui retourne toutes les opérations qui sont de type "Cheque": Vector<Cheque> trouverOperationsCheque()
=> Pour y arriver, il faut utiliser l'opérateur "instanceof", qui est expliqué de façon concise ici:
http://imss-www.upmf-grenoble.fr/prevert/Prog/Java/CoursJava/classes2.html#instanceof

Une explication alternative se trouve ici:
http://www.jmdoudoux.fr/java/dej/chap-poo.htm#poo-2 (Cherchez "4.2.8." sur la page)
=> Testez avec des méthodes de test appropriés.

7. Quand vous appelez la méthode trouverOperations avec le numéro de compte "DE582" en paramètre, retourne-t-elle bien les opérations no. 9559 et 7111? Et pourtant, ces virements concernent bien ce compte (en tant que destinataire). Complétez votre méthode pour que les comptes destinataires des virements soient pris en compte.
=> Ceci nécessite l'utilisation de l'opérateur instanceof et d'un transtypage (angl. "type cast"). Voir section 4.2.8 de http://www.jmdoudoux.fr/java/dej/chap-poo.htm#poo-2
=> Testez avec des méthodes de test appropriés.

8. Définissez une méthode qui renvoie le pourcentage de virements en e-banking.
=> Vous êtes encouragés à augmenter le nombre de données (et les adapter) pour avoir des résultats plus significatifs.
=> Testez avec des méthodes de test appropriés.

9. Créez une méthode qui, pour un numéro de compte donné passé en paramètre (attribut numCptOpé), renvoie la liste des n°s de compte destinataires pour les paiements (paiement = virement débiteur, avec attribut typeOpe = false).
=> Vous êtes encouragés à augmenter le nombre de données (et les adapter) pour avoir des résultats plus significatifs.
=> Testez avec des méthodes de test appropriés.

10. Créez une méthode dans votre classe Banque qui retourne une liste de toutes les opérations qui concernent un compte de prévoyance ou un compte inactif.
Déclaration de la méthode: Vector<Operation> trouverOperationsPourComptesPrevoyanceOuInactif()
=> Réutilisez la méthode trouverOperations définies aux points 5 et 7
=> Créez une méthode qui trouve les comptes qui sont de type prévoyance ou sont inactifs et testez là. Ensuite, utilisez-là. 
