/**
 * @(#)Chien.java
 *
 *
 * @author 
 * @version 1.00 2015/6/3
 */


public class Chien extends Animal {

	String proprietaire;

    public Chien(String proprietaire, double poids) {
    	super(poids, 4);
    	this.proprietaire = proprietaire;
    }
    
    public String decrisToi() {
    	return "Je suis le chien de " + proprietaire + ". " + super.decrisToi() 
    		+ System.lineSeparator() + "Poids moyen par jambe: " + poidsParJambe();
    }
    
    
}