/**
 * @author msteinle 
 * @version 1.00 2015/6/3
 */
public abstract class Animal {

	private double poids;
	private int nombreJambes;

    public Animal(double poids, int nombreJambes) {
    	this.poids = poids;
    	this.nombreJambes = nombreJambes;
    }
    
    public String decrisToi() {
    	return "Mon poids est: " + poids;
    }
    
    public double poidsParJambe() {
    	return poids/nombreJambes;
    }

}