/**
 * @(#)Text1.java
 *
 *
 * @author 
 * @version 1.00 2015/4/23
 */


public class Pays {

	static int sequence = 0;
	
	int id;
	String codeISO;
	
    public Pays(String codeISO) {
    	this.codeISO = codeISO;
    	sequence++;
    	id = sequence;
    }
    
    public String toString() {
    	return "No: " + id + ", codeISO: " + codeISO;
    }
    
    
}