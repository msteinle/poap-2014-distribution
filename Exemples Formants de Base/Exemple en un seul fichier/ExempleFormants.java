/**
 *  ExempleFormants.java
 *
 *
 * AL
 * 
 */

 class ExempleFormants 
 {
    
  /* m�thode main n�cessaire pour l'ex�cution*/
  
    public static void main(String[] args) {
      //
      System.out.println("---------------------------------");
      System.out.println ();
      System.out.println ("EXEMPLE de if");
      System.out.println ();
      ExempleIF();
      System.out.println("---------------------------------");
      System.out.println ();
      //
       System.out.println("---------------------------------");
      System.out.println ();
      System.out.println ("EXEMPLE de switch");
      System.out.println ();
      ExempleSwitch();
      System.out.println("---------------------------------");
      System.out.println ();
      //
      System.out.println("---------------------------------");
      System.out.println ();
      System.out.println ("EXEMPLE de while");
      System.out.println ();
      ExempleWhile();
      System.out.println("---------------------------------");
      System.out.println ();
      //
      System.out.println("---------------------------------");
      System.out.println ();
      System.out.println ("EXEMPLE de do while");
      System.out.println ();
      ExempleDoWhile();
      System.out.println("---------------------------------");
      System.out.println ();
      //
      System.out.println("---------------------------------");
      System.out.println ();
      System.out.println ("EXEMPLE de for");
      System.out.println ();
      ExempleFor();
      System.out.println("---------------------------------");
      System.out.println ();
    }

// Illustration du formant IF

 static void ExempleIF() {
   
		double note = 5.5 ;
		if (note >= 5.5) 	
		{
			System.out.println (" Mention Tr�s Bien");
		}
		else 
		{
			if (note >= 5) 
			{
				System.out.println ("Mention Bien");
			}
				else 
				{
					if (note >= 4) 
					{
						System.out.println ("Promu sans mention");
					}
					else 
					{
						System.out.println ("Non promu");
					}
				}
		} 

	}
	
// Illustration du formant SWITCH

	static void  ExempleSwitch() 
	{	
		System.out.println ("\n Ceci est un exemple d'instruction switch \n \n Cours POAP \n ---------------------- \n" );
		String  maChaine = "DEUXOO";
		switch (maChaine) 
	 	{
			case "Une" : 
			case "une" :
			case "UNE":
				System.out.println ("Vous en avez 1 ");
				break;
			case "Deux" : 
			case "deux" :
			case "DEUX" :
				System.out.println ("Vous en avez 2 ");
				break;
			default :
				System.out.println (" Vous n'avez ni 1 ni 2 ");
		}

	}
	
	// Illustration du formant While
	
	static void ExempleWhile() 
	{
		int  somme = 0;
		int	 bornInf = 0;
		int  bornSup = 20;
		int  compteur  = bornInf;
		
		System.out.println ("nombres pairs de " + bornInf + " � " + bornSup );
		System.out.println();
		
		while ( compteur <= bornSup) 
		{
			System.out.println (compteur );
			somme = somme + compteur;
			compteur += 2 ;	
		}
		
		System.out.println();
		System.out.println (" et la somme est " + somme );
	}
	
	// Illustration du formant DoWhile
	
    static void ExempleDoWhile() 
    {
		int  somme = 0;	
		int  bornInf = 0;
		int  bornSup = 0;
		int  compteur  = 0;
 
/* si on affecte 0 � bornSup la boucle s'ex�cute 1 fois 
    mais une boucle while ne s'ex�cuterait pas.
*/
		System.out.println ();
		System.out.println ("Nombres pairs entre " + bornInf + " et " + bornSup);
		do
		 {
			System.out.println ( compteur );
			somme = somme + compteur;
			compteur +=2 ;	
		} 
		while ( compteur <= bornSup);
	
		System.out.println ();
		System.out.println (" et la somme est " + somme );	

	} 
	

	
// Illustration du formant For

    static void ExempleFor() 
    {

		int bornSup = 12;
		System.out.println ("Table de multiplication de " + bornSup );
		System.out.println ();
		for (int i = 1 ; i <= bornSup; i++) 
		{
			for (int j = 1; j<=bornSup ; j++) 
			{
				System.out.print ( i*j  + "\t");
			}
			System.out.println ();
		}
	}
    
}