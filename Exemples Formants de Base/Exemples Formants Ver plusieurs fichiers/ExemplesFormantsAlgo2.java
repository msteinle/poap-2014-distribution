
public class ExemplesFormantsAlgo2 {

    public static void main (String[] args) {
    	System.out.println ("Exemple If-Else:");
    	System.out.println ("-------------------");
    	ExempleIfElse eI = new ExempleIfElse();
    	eI.run();
    	System.out.println ();
    	//
    	System.out.println ("-------------------");
    	System.out.println ("Exemple Switch:");
    	System.out.println ("-------------------");
    	System.out.println ();
    	ExempleSwitch eS = new ExempleSwitch();
    	eS.run();
    	
    	//
    	System.out.println ("-------------------");
    	System.out.println ("Exemple While:");
    	System.out.println ("-------------------");
    	ExempleWhile eW = new ExempleWhile();
    	eW.run();
    	System.out.println ();
    	//
    	System.out.println ("-------------------");
    	System.out.println ("Exemple Do While:");
    	System.out.println ("-------------------");
    	System.out.println ();
    	ExempleDoWhile eD = new ExempleDoWhile();
    	eD.run();
    	System.out.println ();
    	//
    	System.out.println ("-------------------");
    	System.out.println ("Exemple For:");
    	System.out.println ("-------------------");
    	System.out.println ();
    	ExempleFor eF = new ExempleFor();
    	eF.run();
    	System.out.println ();
    
	}
    
}