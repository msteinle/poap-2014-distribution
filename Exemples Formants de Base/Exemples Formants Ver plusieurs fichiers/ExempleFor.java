
public class ExempleFor {

    public void run() 
	{
		int bornSup = 12;
		System.out.println ("Table de multiplication de " + bornSup );
		System.out.println ();
		for (int i = 1 ; i <= bornSup; i++) 
		{
			for (int j = 1; j<=bornSup ; j++) 
			{
				System.out.print ( i*j  + "\t");
			}
			System.out.println ();
		}
	}
    
}