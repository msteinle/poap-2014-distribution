
public class ExempleSwitch {

    public void run() 
	{
		System.out.println ("\n Ceci est un exemple d'instruction switch \n \n Cours POAP \n ---------------------- \n" );
		String  maChaine = "DEUXOO";
		switch (maChaine) 
	 	{
			case "Une" : 
			case "une" :
			case "UNE":
				System.out.println ("Vous en avez 1 ");
				break;
			case "Deux" : 
			case "deux" :
			case "DEUX" :
				System.out.println ("Vous en avez 2 ");
				break;
			default :
				System.out.println (" Vous n'avez ni 1 ni 2 ");
		}

	}
    
}