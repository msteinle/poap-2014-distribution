
public class ExempleIfElse {

	public void run() 
	{
		double note = 5.5 ;
		if (note >= 5.5) 	
		{
			System.out.println (" Mention Tr�s Bien");
		}
		else if (note >= 5) 
		{
			System.out.println ("Mention Bien");
		}
		else if (note >= 4) 
		{
			System.out.println ("Promu sans mention");
		}
		else 
		{
			System.out.println ("Non promu");
		}

	}
}
