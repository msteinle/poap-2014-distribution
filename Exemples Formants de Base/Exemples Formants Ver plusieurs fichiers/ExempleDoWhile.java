
public class ExempleDoWhile {
    
	public void run() 
	{
		int  somme = 0;	
		int  bornInf = 0;
		int  bornSup = 0;
		int  compteur  = 0;
	 
		// si on affecte 0 � bornSup la boucle s'ex�cute 1 fois 
	    // mais une boucle while ne s'ex�cuterait pas.
		System.out.println ();
		System.out.println ("Nombres pairs entre " + bornInf + " et " + bornSup);
		do
		 {
			System.out.println ( compteur );
			somme = somme + compteur;
			compteur +=2 ;	
		} 
		while ( compteur <= bornSup);
		
		System.out.println ();
		System.out.println (" et la somme est " + somme );	

	}
}