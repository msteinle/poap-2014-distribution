
public class ExempleWhile {

    public void run()
	{
		int  somme = 0;
		int	 bornInf = 0;
		int  bornSup = 20;
		int  compteur  = bornInf;
		
		System.out.println ("nombres pairs de " + bornInf + " � " + bornSup );
		System.out.println();
		
		while ( compteur <= bornSup) 
		{
			System.out.println (compteur );
			somme = somme + compteur;
			compteur += 2 ;	
		}
		
		System.out.println();
		System.out.println (" et la somme est " + somme );
	}
    
}