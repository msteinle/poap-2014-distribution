import greenfoot.*;  // imports Actor, World, Greenfoot, GreenfootImage

import java.util.Scanner;
import java.util.Random;
import java.io.File;


/**
 * A garden where hedgehogs live.
 * 
 * @author Michael Kölling
 * @version 1.0
 */
public class Garden extends World
{   
    /**
     * Create a new world with 8x8 cells and
     * with a cell size of 60x60 pixels
     */
    public Garden() 
    {
        super(8, 8, 60);
        setPaintOrder(Hedgehog.class, Apple.class);

        prepare();
    }

    /**
     * Place a number of apples into the world at random places.
     * The number of apples can be specified.
     */
    public void randomApples(int howMany)
    {
        for(int i=0; i<howMany; i++) {
            Apple apple = new Apple();
            int x = Greenfoot.getRandomNumber(getWidth());
            int y = Greenfoot.getRandomNumber(getHeight());
            addObject(apple, x, y);
        }
    }

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    { 
        //LECTURE DU FICHIER        
        try{
            
            File fichier = new File("donneesHA.txt");
            Scanner sc = new Scanner(fichier);
            
            
            for (int i = 0; i < 9; i++){
               
				String type = sc.next();
				type = type.toLowerCase();
				if(type.equals("hedgehog")){
					String nom = sc.next();
					int x = sc.nextInt();
					int y = sc.nextInt();
					
					Hedgehog hedgehog3 = new Hedgehog(nom);
					addObject(hedgehog3, x, y);
				}
				else{
					int x1 = sc.nextInt();
					int y1 = sc.nextInt();
					
					Apple apple1 = new Apple();
					addObject(apple1, x1, y1);
				}
			}
		}
        catch(Exception ex) {
            ex.printStackTrace();
        }   
    }
}