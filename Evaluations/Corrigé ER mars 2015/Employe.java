/*
 *noms des champs pour copier-coller :
empId
empNom
empPrenom
empAVS
empSalaireAnnuel
empAnnee
empTempsPlein
empPourcentTravail
*/

class Employe
{
	int	empId;
	String empNom, empPrenom, empAVS;
	double empSalaireAnnuel;
	int empAnnee;
	boolean empTempsPlein;
	int empPourcentTravail;
	
	Employe(int	empId, String empNom, String empPrenom, String empAVS, double empSalaireAnnuel, int empAnnee, boolean empTempsPlein, int empPourcentTravail)
	{
		this.empId = empId;
		this.empNom = empNom;
		this.empPrenom = empPrenom;
		this.empAVS = empAVS;
		this.empSalaireAnnuel = empSalaireAnnuel;
		this.empAnnee = empAnnee;
		this.empTempsPlein = empTempsPlein;
		this.empPourcentTravail = empPourcentTravail;
	}
	
	Employe(String ligne)
	{
		java.util.Scanner flux = new java.util.Scanner(ligne);
		
		empId = flux.nextInt();
		empNom = flux.next();
		empPrenom = flux.next();
		empAVS = flux.next();
		empSalaireAnnuel = flux.nextDouble();
		empAnnee = flux.nextInt();
		empTempsPlein = flux.nextBoolean();
		empPourcentTravail = 100;
		if ( !empTempsPlein )
			empPourcentTravail = flux.nextInt();
	}
	
	public String toString()
	{
			return empPrenom + " "+empNom+" : employ�(e) n�"+empId+" depuis "+empAnnee+" � "+
				   (empTempsPlein?"temps plein":empPourcentTravail+"%")+" pour "+empSalaireAnnuel+" CHF annuels";
	}
}