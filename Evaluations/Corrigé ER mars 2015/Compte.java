/**
 * mars 2015
 *
 * @author Mirko Steinle + Eric Batard
 */
 
class Compte
{
	/* les 3 constantes pour les 3 types de compte */
    public static final int COMPTE_EPARGNE = 1, COMPTE_COURANT = 2, COMPTE_PREVOYANCE = 3;

    /* private */ String noCpt;
    /* private */ double soldeCpt;
    /* private */ double limiteCreditCpt;
    /* private */ boolean actifCpt;
    /* private */ int typeCpt;


    public Compte(String numeroCompte, double soldeCompte, double limiteCreditCompte, boolean actifCompte, int typeCompte)
    {
        noCpt = numeroCompte;
        soldeCpt = soldeCompte;
        limiteCreditCpt = limiteCreditCompte;
        actifCpt = actifCompte;
        typeCpt = typeCompte;
    }


    public boolean crediter(double montant)
    {
        if (montant < 0)
        {
            return false;
        }
        else
        {
	        soldeCpt = soldeCpt + montant;
	        return true;
        }
    }


    public boolean debiter(double montant)
    {
        if (typeCpt == COMPTE_PREVOYANCE)
        {
            return false;
        }
        else if (montant < 0)
        {
            return false;
        }
        // PAS : (soldeCpt - montant < limiteCreditCpt)
        // MAIS montant - soldeCpt >= limiteCreditCpt
        // OU :
        else if (soldeCpt - montant < -limiteCreditCpt)
        {
            return false;
        }
        else
        {
	        soldeCpt = soldeCpt - montant;
	        return true;
        }
    }


/* je compte supprimer :   @Override*/
    public String toString()
    {
        return "Compte< num. '" + noCpt + "', solde = " + soldeCpt +" >";
    }

    /**********************************************/
    /* A SUPPRIMER POUR L'EPREUVE     !!!!!!!!!!!!*/
    /**********************************************/
    
    
    public String toStringDemand�()
    {
        return "Compte "+typeEnClair()+"< num. '" + noCpt + "' "+(actifCpt?"":"(INACTIF)")+" , solde = " + soldeCpt +" CHF >";
    }
    
    private String typeEnClair()
    {
    	switch ( typeCpt )
    	{
    		case COMPTE_EPARGNE : return "Epargne"; // pas de break car unreachable statement
    		case COMPTE_COURANT : return "Courant"; 
    		case COMPTE_PREVOYANCE : return	"Pr�voyance";
    		default : return null; // obligatoire car risque de pas de return
    	}
    }
    
    boolean solderPrevoyance(Compte compteDest)
    {
    	if ( typeCpt != COMPTE_PREVOYANCE )
    	{
    		System.out.println ("Erreur - op�ration impossible sur ce type de compte");
    		return false;
    	}
    	else if ( compteDest.typeCpt != COMPTE_COURANT )
    	{
    		System.out.println ("Erreur - virement impossible sur ce type de compte");
    		return false;
    	}
    	else/* on dira que c'est atomique :-)) */
    	{
    		compteDest.crediter(soldeCpt);
    		soldeCpt = 0;
    		actifCpt = false;
    		return true;
    	}
     }
}
