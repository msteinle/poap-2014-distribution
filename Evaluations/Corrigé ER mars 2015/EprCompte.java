import java.util.*;
import java.io.*;

class EprCompte
{
	public static final int NOMBRE_COMPTE = 12;
	public static void main (String[] args)
	{
		/* le tableau tabCompte est initialisé par la méthode remplirTableau */
		Compte[] tabCompte = remplirTableau("ListeCompte.txt");
		
		afficherTableau(tabCompte);
		
		// à supprimer pour l'épreuve
		afficherTableauAvecToStringDemandé(tabCompte);
		
		System.out.println (); System.out.println ();
		repartitionComptes(tabCompte);
		
		System.out.println (); System.out.println ();
//		ancienPreleverDansListe(10, tabCompte);
		int nbErreurs = preleverDansListe(10, tabCompte); 		
		afficherTableauAvecToStringDemandé(tabCompte);
		System.out.println();
		System.out.println ("Nombre de comptes non modifiés (dépassement) : "+nbErreurs);
					
		
	}
	
/* ne pas modifier ces méthodes */
/**/	private static void afficherTableau(Compte[] tabCompte)
/**/	{
/**/		for (int position = 0; position < tabCompte.length; position++)
/**/		{
/**/			System.out.println (tabCompte[position]);
/**/		}
/**/	}
/**/	
/**/	private static Compte[] remplirTableau(String nomFichier)
/**/	{
/**/		Compte[] résultat = new Compte[NOMBRE_COMPTE];
/**/			
/**/		try
/**/		{
/**/			Scanner fluxFichier = new Scanner(new File(nomFichier));
				
				int position = 0;
				
				while ( fluxFichier.hasNext() )
				{
					résultat[position] = traiterLigne(fluxFichier.nextLine());
					position++;
				}
/**/		}
			catch (IOException ioe)
			{
				System.out.println (ioe);
			}
			return résultat;
/**/	}

		private static Compte traiterLigne(String ligne)
		{
			Scanner fluxLigne = new Scanner(ligne);
			return new Compte(fluxLigne.next(), fluxLigne.nextDouble(), fluxLigne.nextDouble(), 
				              fluxLigne.nextBoolean(), fluxLigne.nextInt());
		}
/**/	

		private static void repartitionComptes (Compte[] t)
		{
			int nbActifs = 0;
			double montantTotalActifs = 0;
			double montantTotalInactifs = 0;
			
			for ( int i = 0; i < t.length; i++ )
			{
				if ( t[i].actifCpt )
				{
					nbActifs++;
					montantTotalActifs += t[i].soldeCpt;
				}
				else
					montantTotalInactifs += t[i].soldeCpt;
			}
			
			/* affichage à compléter */
			System.out.println ("Nombre de comptes actifs "+nbActifs+ " pour un montant total de "+montantTotalActifs+" CHF");
			System.out.println ("Nombre de comptes inactifs "+(t.length-nbActifs)+ " pour un montant total de "+montantTotalInactifs+" CHF");
		}
		
	private static void afficherTableauAvecToStringDemandé(Compte[] tabCompte)
	{
		for (int position = 0; position < tabCompte.length; position++)
		{
			System.out.println (tabCompte[position].toStringDemandé());
		}
	}

		
	private static boolean ancienPreleverDansListe(double montantFrais, Compte[] t)
	{
		boolean erreur = false;
		
		for ( int i = 0; i <  t.length; i++ )
		{
			if ( t[i].typeCpt != Compte.COMPTE_PREVOYANCE )
				if ( !t[i].debiter(montantFrais) )
				{
					System.out.println ("Impossible de percevoir les frais - compte :"+t[i].noCpt);
					erreur = true;
				}
		}
		return !erreur;
	}	
		
	private static int preleverDansListe(double montantFrais, Compte[] t)
	{
		int nbErreur = 0;
		
		for ( int i = 0; i <  t.length; i++ )
		{
			if ( t[i].typeCpt != Compte.COMPTE_PREVOYANCE )
				if ( !t[i].debiter(montantFrais) )
				{
					System.out.println ("Impossible de percevoir les frais - compte :"+t[i].noCpt);
					nbErreur++;
				}
		}
		return nbErreur;
	}	
}

