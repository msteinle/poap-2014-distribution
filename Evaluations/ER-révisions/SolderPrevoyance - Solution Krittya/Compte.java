/******************************/
/* BLAISE Marie-Jos� Candida  */
/******************************/

 
class Compte
{
	/* les 3 constantes pour les 3 types de compte */
    public static final int COMPTE_EPARGNE = 1, COMPTE_COURANT = 2, COMPTE_PREVOYANCE = 3;

    /* private */ String noCpt;
    /* private */ double soldeCpt;
    /* private */ double limiteCreditCpt;
    /* private */ boolean actifCpt;
    /* private */ int typeCpt;


    public Compte(String numeroCompte, double soldeCompte, double limiteCreditCompte, boolean actifCompte, int typeCompte)
    {
        noCpt = numeroCompte;
        soldeCpt = soldeCompte;
        limiteCreditCpt = limiteCreditCompte;
        actifCpt = actifCompte;
        typeCpt = typeCompte;
        
        
        
    }

    public boolean crediter(double montant)
    {
        if (montant < 0)
        {
            return false;
        }
        else
        {
	        soldeCpt = soldeCpt + montant;
	        return true;
        }
    }

    public boolean debiter(double montant)
    {
        if (typeCpt == COMPTE_PREVOYANCE)
        {
            return false;
        }
        else if (montant < 0)
        {
            return false;
        }
        else if (soldeCpt - montant < -limiteCreditCpt)
        {
            return false;
        }
        else
        {
	        soldeCpt = soldeCpt - montant;
	        return true;
        }
    }

    public String toString()
    {
    	
    	//Recherche si le compte est actif ou inactif
    	String etatCompte;
        if (actifCpt == true)
        {
        	etatCompte = " " ;
        }
        else
        {
        	etatCompte = "(INACTIF)";
        }
        
        //Changement pour l'affichage des types de comptes
        String typeCompte;
        if(typeCpt == 1)
        {
        	typeCompte = "Epargne";
        }
        else if (typeCpt == 2)
        {
        	typeCompte = "Courant";
        }
        else
        {
        	typeCompte = "Pr�voyance";
        }
        
        return "Compte "+ typeCompte + "< num. '" + noCpt + etatCompte + "', solde = " + soldeCpt +" >";
    }
    
    
    
    
    public boolean solderPrevoyance(Compte unCompte)
    {	
    	if (typeCpt == COMPTE_PREVOYANCE)
    	{
    		if (unCompte.typeCpt == COMPTE_COURANT)
    		{
    			unCompte.crediter(soldeCpt);
    			soldeCpt = 0;
    			actifCpt = false;

    			System.out.println ("Le compte est sold�");
    			
    			return true;    
    		}
    		else
    		{
    			System.out.println ("Ce n'est pas un compte courant, le compte ne peut �tre cr�dit�");
    			return false;
    		}
    		
    	}
    	else
    	{
    		System.out.println ("Ce n'est pas un compte de pr�voyance, le compte ne peut �tre sold�");
    		return false;	
    	}
    }
    
    
}//fin de la class Compte
