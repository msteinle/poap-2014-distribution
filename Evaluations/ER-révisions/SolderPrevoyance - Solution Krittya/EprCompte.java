/******************************/
/* BLAISE Marie-Jos� Candida  */
/******************************/



import java.util.*;
import java.io.*;

class EprCompte
{
	public static final int NOMBRE_COMPTE = 12;
	public static void main (String[] args)
	{
		/* le tableau tabCompte est initialis� par la m�thode remplirTableau */
		Compte[] tabCompte = remplirTableau("ListeCompte.txt");
		
		afficherTableau(tabCompte);
		
/*
		System.out.println (); System.out.println ();
		repartitionComptes(tabCompte);
		
		System.out.println (); System.out.println ();
		int nombre = preleverDansListe(tabCompte, 10);
		
		System.out.println ();
		afficherTableau(tabCompte);
		
		System.out.println ();
		System.out.print("Nombre de compte nom modifi�s (d�passement) : "+ nombre);*/
		
		
		System.out.println (); System.out.println ();
		testComptesActifs(tabCompte);
		/*
		System.out.println ();
		afficherTableau(tabCompte);
		
		System.out.println ();
		solderListePrev("FinPrev.txt", tabCompte);
		
		System.out.println (); System.out.println ();
		solderListePrev("FinPrev.txt",tabCompte);*/
	}
	
	
	
/***************** ne pas modifier ces m�thodes */
/**/	private static void afficherTableau(Compte[] tabCompte)
/**/	{
/**/		for (int position = 0; position < tabCompte.length; position++)
/**/		{
/**/			System.out.println (tabCompte[position]);
/**/		}
/**/	}
/**/	
/**/	private static Compte[] remplirTableau(String nomFichier)
/**/	{
/**/		Compte[] r�sultat = new Compte[NOMBRE_COMPTE];
/**/			
/**/		try
/**/		{
/**/			Scanner fluxFichier = new Scanner(new File(nomFichier));
/**/				
/**/			int position = 0;
/**/				
/**/			while ( fluxFichier.hasNext() )
/**/			{
/**/				r�sultat[position] = traiterLigne(fluxFichier.nextLine());
/**/				position++;
/**/			}
/**/		}
/**/		catch (IOException ioe)
/**/		{
/**/			System.out.println (ioe);
/**/		}
/**/		return r�sultat;
/**/	}
/**/
/**/	private static Compte traiterLigne(String ligne)
/**/	{
/**/		Scanner fluxLigne = new Scanner(ligne);
/**/		return new Compte(fluxLigne.next(), fluxLigne.nextDouble(), fluxLigne.nextDouble(), 
/**/				              fluxLigne.nextBoolean(), fluxLigne.nextInt());
/**/	}
/***********************************************************************************/	



	static void repartitionComptes(Compte[] tabCompte)
	{
		int nombreActif = 0;
		double sommeActif = 0;
		int nombreInactif = 0;
		double sommeInactif = 0;
		
		for (int i = 0; i<tabCompte.length; i++)
		{
			Compte c = tabCompte[i];
			
			if(c.actifCpt == true)
			{
				nombreActif++;
				sommeActif = sommeActif + c.soldeCpt;
			}
			
			if(c.actifCpt == false)
			{
				nombreInactif++;
				sommeInactif = sommeInactif + c.soldeCpt;
			}
		}	
		System.out.println ("Nombre de comptes actifis : " + nombreActif + " pour un montant total de " + sommeActif + " CHF");	
		System.out.println ("Nombre de comptes inactifis : " + nombreInactif + " pour un montant total de " + sommeInactif + " CHF");	
	}//fin du void repartitionComptes
	
	
	static int preleverDansListe(Compte[] tabCompte, double montant)
	{
		int nombre = 0;
		for (int i = 0; i<tabCompte.length; i++)
		{
			Compte c = tabCompte[i];
			if(c.typeCpt < Compte.COMPTE_PREVOYANCE)
			{
				if(!c.debiter(montant))
				{
					System.out.println ("Impossible de percevoir les frais - compte : " + c.noCpt);
					nombre++;
				}	
			}
		}
		return nombre;
	}
	
	
	
	
	
	
	static void testComptesActifs(Compte[] tabCompte)
	{
		tabCompte[4].solderPrevoyance(tabCompte[1]); 
			
	}
	
	
	
	
	
	static void solderListePrev(String listePrev, Compte[] tabCompte)
	{
		Vector<Compte> v = new Vector<Compte>();
		
		for (int i = 0; i<tabCompte.length; i++)
		{
			v.add(tabCompte[i]);
			System.out.println (v.get(i));
		}
		try
		{
			Scanner sc = new Scanner(new File(listePrev));	
				
			int position = 0;
					
			while ( sc.hasNext() )
				{
					int prevoyance = sc.nextInt ();
					int courant = sc.nextInt ();
					
					v.get(prevoyance).solderPrevoyance(v.get(courant)); 
				}	
		}
		
		catch (IOException ioe)
		{
			System.out.println (ioe);	
		}
	}

}//fin de la class

