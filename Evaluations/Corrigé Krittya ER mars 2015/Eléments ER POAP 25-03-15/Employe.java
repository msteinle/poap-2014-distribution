/****************************/
/* NOIPHENG Krittiya     */
/****************************/



/*
 *noms des champs pour copier-coller :
empId
empNom
empPrenom
empAVS
empSalaireAnnuel
empAnnee
empTempsPlein
empPourcentTravail
*/
class Employe {
	
	int empId;
	String empNom;
	String empPrenom;
	String empAVS;
	double empSalaireAnnuel;
	int empAnnee;
	boolean empTempsPlein;
	int empPourcentTravail;
	
	
	
	public Employe(int pEmpId, String pEmpNom, String pEmpPrenom, String pEmpAVS, double pEmpSalaireAnnuel, int pEmpAnnee, boolean pEmpTempsPlein, int pEmpPourcentTravail ){
		empId = pEmpId;
		empNom = pEmpNom;
		empPrenom = pEmpPrenom;
		empAVS = pEmpAVS;
		empSalaireAnnuel = pEmpSalaireAnnuel;
		empAnnee = pEmpAnnee;
		empTempsPlein = pEmpTempsPlein;
		empPourcentTravail = pEmpPourcentTravail;
		
		
	}//constructeur
	
	public String toString (){
		String affichage;
		
		affichage = empPrenom + " " + empNom + " : employ�(e) n�" + empId + " depuis " + empAnnee;
		
		if(empTempsPlein == true){
			affichage += " � plein temps ";
		}else{
			affichage += " � " + empPourcentTravail + "%";
		}
		
		affichage += " pour " + empSalaireAnnuel + " CHF annuels";
		
		return affichage;
	}
	
}//class