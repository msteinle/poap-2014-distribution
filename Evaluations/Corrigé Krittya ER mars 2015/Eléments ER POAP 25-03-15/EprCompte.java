/****************************/
/* NOIPHENG Krittiya    */
/****************************/



import java.util.*;
import java.io.*;

class EprCompte
{
	public static final int NOMBRE_COMPTE = 12;
	public static void main (String[] args)
	{
		/* le tableau tabCompte est initialis� par la m�thode remplirTableau */
		Compte[] tabCompte = remplirTableau("ListeCompte.txt");
		
		afficherTableau(tabCompte);

		System.out.println (); System.out.println ();
		/* ici appel de repartitionComptes */
		repartitionComptes(tabCompte);
		
		System.out.println (); System.out.println ();
		/* ici appel de preleverDansListe 
		   et de afficherTableau */
		double somme = 10;
		int nbCptNonModif = preleverDansListe(tabCompte, somme);
		if(nbCptNonModif > 0){
			afficherTableau(tabCompte);
			System.out.println (System.lineSeparator() + "Nombre de comptes non modifi�s (d�passement) : " + nbCptNonModif);
		}
	}
	
	public static void repartitionComptes(Compte[] tabCompte){
		int nbCompteActif = 0;
		int nbCompteInactif = 0;
		double montantTotalActif = 0;
		double montantTotalInactif = 0;
		
		for (int i = 0; i< tabCompte.length ; i++){
			Compte c = tabCompte[i];
			if(c.actifCpt == true){
				nbCompteActif++;
				montantTotalActif += c.soldeCpt;
			}else{
				nbCompteInactif++;
				montantTotalInactif += c.soldeCpt;
			}	
		}
		System.out.println ("Nombre de compte actifs : " + nbCompteActif + " pour un montant total de " + montantTotalActif + " CHF");
		System.out.println ("Nombre de compte inactifs : " + nbCompteInactif + " pour un montant total de " + montantTotalInactif + " CHF");		
		
	}//repartitionComptes
	
	public static int preleverDansListe(Compte[] tabCompte, double somme){
		double prelevement=0;
		int nbCptNonModif = 0;
		
		for (int i = 0; i< tabCompte.length ; i++){
			Compte c = tabCompte[i];
			if((c.typeCpt != c.COMPTE_PREVOYANCE)){
				boolean succes = c.debiter(somme);
				if(!succes){
					System.out.println ("Impossible de percevoir les frais - compte : " + c.noCpt + System.lineSeparator());
					nbCptNonModif ++;	
				}	
			}
		}
		return nbCptNonModif;
	} //preleverDansListe
	
	
	
/***************** ne pas modifier ces m�thodes */
/**/	private static void afficherTableau(Compte[] tabCompte)
/**/	{
/**/		for (int position = 0; position < tabCompte.length; position++)
/**/		{
/**/			System.out.println (tabCompte[position]);
/**/		}
/**/	}
/**/	
/**/	private static Compte[] remplirTableau(String nomFichier)
/**/	{
/**/		Compte[] r�sultat = new Compte[NOMBRE_COMPTE];
/**/			
/**/		try
/**/		{
/**/			Scanner fluxFichier = new Scanner(new File(nomFichier));
/**/				
/**/			int position = 0;
/**/				
/**/			while ( fluxFichier.hasNext() )
/**/			{
/**/				r�sultat[position] = traiterLigne(fluxFichier.nextLine());
/**/				position++;
/**/			}
/**/		}
/**/		catch (IOException ioe)
/**/		{
/**/			System.out.println (ioe);
/**/		}
/**/		return r�sultat;
/**/	}
/**/
/**/	private static Compte traiterLigne(String ligne)
/**/	{
/**/		Scanner fluxLigne = new Scanner(ligne);
/**/		return new Compte(fluxLigne.next(), fluxLigne.nextDouble(), fluxLigne.nextDouble(), 
/**/				              fluxLigne.nextBoolean(), fluxLigne.nextInt());
/**/	}
/***********************************************************************************/	

}//class

