/****************************/
/* NOIPHENG Krittiya   */
/****************************/

 
class Compte
{
	/* les 3 constantes pour les 3 types de compte */
    public static final int COMPTE_EPARGNE = 1, COMPTE_COURANT = 2, COMPTE_PREVOYANCE = 3;

    /* private */ String noCpt;
    /* private */ double soldeCpt;
    /* private */ double limiteCreditCpt;
    /* private */ boolean actifCpt;
    /* private */ int typeCpt;


    public Compte(String numeroCompte, double soldeCompte, double limiteCreditCompte, boolean actifCompte, int typeCompte)
    {
        noCpt = numeroCompte;
        soldeCpt = soldeCompte;
        limiteCreditCpt = limiteCreditCompte;
        actifCpt = actifCompte;
        typeCpt = typeCompte;
    }

    public boolean crediter(double montant)
    {
        if (montant < 0)
        {
            return false;
        }
        else
        {
	        soldeCpt = soldeCpt + montant;
	        return true;
        }
    }

    public boolean debiter(double montant)
    {
        if (typeCpt == COMPTE_PREVOYANCE)
        {
            return false;
        }
        else if (montant < 0)
        {
            return false;
        }
        else if (soldeCpt - montant < -limiteCreditCpt)
        {
            return false;
        }
        else
        {
	        soldeCpt = soldeCpt - montant;
	        return true;
        }
    }

	
    public String toString()
    {	
    	String resultat;
        
        if (typeCpt == COMPTE_EPARGNE){
        	resultat = "Compte Epargne ";
        }else if (typeCpt == COMPTE_COURANT){
        	resultat = "Compte Courant ";
        }else{
        	resultat = "Compte Prévoyance ";
        }
        
        resultat += "< num. '" + noCpt + "' ";
        
        if (actifCpt == false){
        	resultat += " (INACTIF), ";	
        }
        resultat += "solde = " + soldeCpt +" >";
        	
        return resultat;
    }
}
