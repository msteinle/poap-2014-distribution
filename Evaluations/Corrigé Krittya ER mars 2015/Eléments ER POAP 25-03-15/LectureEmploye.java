/**
 * @(#)LectureEmploye.java
 *
 *
 * @NOIPHENG Krittiya 
 * @version 1.00 2015/3/25
 */
 
import java.util.Scanner;
import java.io.File;

public class LectureEmploye {
    final static int NOMBRE_EMPLOYE = 12;    
    	
    public static void main(String[] args) {
        Employe[] lesEmployes = lireLesEmployes("DonnéesEmployés.txt");
        
    	afficherEmp(lesEmployes);
    	
    }//main
    
    public static Employe lireUnEmploye(String ligneEmp){
    	Scanner sc = new Scanner(ligneEmp);
    	
    	int id = sc.nextInt();
    	String nom = sc.next();
    	String prenom = sc.next();
    	String avs = sc.next();
    	double salaireAnn = sc.nextDouble();
    	int annee = sc.nextInt();
    	boolean tempsPlein = sc.nextBoolean();
    	int ptgeTravail;
    	
    	if(tempsPlein == false){
    		ptgeTravail = sc.nextInt();
    		
    	}else{
    		ptgeTravail = 100;
    	}
    	
    	Employe nouveauEmp = new Employe(id, nom, prenom, avs, salaireAnn, annee, tempsPlein, ptgeTravail);
    	
    	return nouveauEmp;
    }//lireUnEmploye
    
    public static Employe[] lireLesEmployes(String nomDuFichier){
    	Employe[] employe = new Employe[NOMBRE_EMPLOYE];
    	
    	try{
    		File fichier = new File(nomDuFichier);
        	Scanner sc = new Scanner(fichier);
        	
        	for (int i = 0; i< employe.length; i++){
        		String uneLigne = sc.nextLine();
        		employe[i] = lireUnEmploye(uneLigne);
        	}
        		
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return employe;
    }//lirelesEmployes
    
	public static void afficherEmp(Employe[] employe){
		for (int i = 0; i< employe.length; i++){
            System.out.println (employe[i]);
        }
	}//afficherEmp
	
}//class
