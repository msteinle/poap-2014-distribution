/**
 * @(#)Film.java
 *
 *
 * @author Bruno Carvas Martins
 * @version 1.00 2015/3/15
 */
 
/*ETAPE 1*/
public class Film 
{
	
	int identifiantFilm;
	String nom_film;
	String nom_realisateur;
	int annee_film;
	double budget;
	int note_public;
	boolean nationalite;

/*ETAPE 2*/
    public Film(int pIdentifiant, String pNom_film, String pNom_realisateur, int pAnnee_film, double pBudget, int pNote_public, boolean pNationalite) {
    	
    	identifiantFilm = pIdentifiant;
    	nom_film = pNom_film;
    	nom_realisateur = pNom_realisateur;
    	annee_film = pAnnee_film;
    	budget = pBudget;
    	note_public = pNote_public;
    	nationalite = pNationalite;
    	
    }
    /*ETAPE 3*/
    public String toString()
    {
    	String text = "Identifiant du film : " + identifiantFilm + ". Nom de film : " + nom_film + ". Nom du r�alisateur : " + nom_realisateur +  ". Ann�e de r�alisation : " + annee_film ;
    	String text2 = " . Budget : " + budget + ". Note du public : " + note_public + ". Nationalit� : ";
    	if (annee_film <1960)
    	{
    		text = text + " (vieux film)" + text2;
    	}
    	else
    	{
    		text = text + text2;
    	}
    	if(nationalite==true)
    	{
    		text = text + "Suisse";			
    	}
    	else 
    	{
    		text = text + "Etranger";
    	}
   		return text;
   	}
    
}