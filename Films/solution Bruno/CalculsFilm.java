/**
 * @(#)CalculsFilm.java
 *
 *
 * @author Bruno Carvas Martins
 * @version 1.00 2015/3/15
 */

/*DECLARATION DES IMPORTS NECESSAIRES POUR L'EXECUTION*/
import java.io.FileNotFoundException;
import java.lang.Exception;
import java.lang.String;
import java.lang.System;
import java.util.Scanner;
import java.io.File;

public class CalculsFilm {

	/*DECLARATION D'UNE CONSTANTE*/
    final static int NOMBRE_FILMS = 10;
    
    /*METHODE MAIN, APPEL A TOUTES LES AUTRES METHODES CONTENUES DANS CE FICHIER*/
    public static void main (String[] args) {
    	
    	Film[] lesFilms = lireFilms();
    	afficherFilms(lesFilms);
    	
    	int note_min = calculerNoteMin(lesFilms);
    	double budget_max = calculerBudgetMax(lesFilms);
    	afficherNomsFilmsNoteMinimale(lesFilms);
    	double budget_total = calculerBudgetTotalFilmsSuisses(lesFilms);
    	afficherNomsFilmsRealisateur(lesFilms,"CAPRI");
    	afficherNomsFilmsSuissesApresUneDate(lesFilms, 2005);
    	afficherNomsFilmsSuissesAvecBudgetSuperieur(lesFilms, 25000);
    	
    	
    	/*AFFICHAGE*/
    	System.out.println ();
    	
    	/*DE L'ETAPE 7*/
    	System.out.println ("La note minimale obtenue par un ou plusieurs films est " + note_min + "." + System.lineSeparator());
    	System.out.println ("Le budget maximal pour la r�alisation d'un film est de " + budget_max + " CHF." + System.lineSeparator());
    	
    	/*DE L'ETAPE 9*/
    	System.out.println ("Le budget total des films suisses est de " + budget_total + ".");
    	
	}
    /*ETAPE 4 et 6*/
    static Film[] lireFilms()
    {
        Film[] films = new Film[NOMBRE_FILMS];
        try {

            File fichier = new File("Film.txt"); /*ETAPE 5 Cr�ation de Film.txt*/
            Scanner sc = new Scanner(fichier);

            for(int i = 0; i < films.length; i++) {

                //Lecture
                int idFil = sc.nextInt();
				String nomFil = sc.next();
				String nomrealFil = sc.next();
				int annFil = sc.nextInt();
				double budgFil = sc.nextDouble();
				int notFil = sc.nextInt();
				boolean natFil = sc.nextBoolean();

                //Cr�ation d'un nouveau film
                Film nouveauFil = new Film(idFil, nomFil, nomrealFil, annFil, budgFil, notFil, natFil);

                //Mettre le film dans un tableau
                films[i] = nouveauFil;

            }
        } catch(Exception ex) {
            // Afficher l'exception
            ex.printStackTrace();
        }
        return films;
    }
    
    static void afficherFilms(Film[] nosFilms)
    {
        for (int i = 0; i< nosFilms.length; i++){
           	System.out.println (nosFilms[i]);
        }
    }
    
    /*ETAPE 7*/
    static int calculerNoteMin(Film[] lesFilms)
    {
    	Film premierFilm = lesFilms[0];
    	int note_min = Integer.MAX_VALUE;
    	
    	for (int i = 0; i<lesFilms.length; i++){
    		
    		if (lesFilms[i].note_public <= note_min){
    			note_min = lesFilms[i].note_public;
    		}
    	}
    	return note_min;
    }
    
    static double calculerBudgetMax(Film[] lesFilms)
    {
    	Film premierFilm = lesFilms[0];
    	double budget_max = Double.MIN_VALUE;
    	
    	for (int i = 0; i<lesFilms.length; i++){
    		
    		if (lesFilms[i].budget > budget_max){
    			budget_max = lesFilms[i].budget;
    		}
    	} 
    	return budget_max;
    }
    
    /*ETAPE 8*/
    static void afficherNomsFilmsNoteMinimale(Film[] lesFilms)
    {
    	Film premierFilm = lesFilms[0];
    	//msteinle: ici il faudrait utiliser la note minimale parmi les films pass�s en param�tre. Concr�tement, il faut vous servir de la m�thode calculerNoteMin que vous avez �crit ci-dessus//
    	int films_note_min = calculerNoteMin(lesFilms);
  		
  		System.out.println ();
    	System.out.println ("Les films ayant obtenus la note minimale sont : " + System.lineSeparator());
    	
    	for (int i = 0; i<lesFilms.length; i++){
    		if (lesFilms[i].note_public == films_note_min){
    			
    			/*AFFICHAGE*/
    			System.out.println (lesFilms[i]);
    		} 
    	}
    }
    
    /*ETAPE 9*/
    static double calculerBudgetTotalFilmsSuisses (Film[] lesFilms)
    {
    
    	double budget_total = 0;
    	
    	System.out.println ();
    	
    	for (int i = 0; i<lesFilms.length; i++)
    	{
    		Film f = lesFilms[i];
    		if (f.nationalite == true)
    		{
    			budget_total = budget_total + f.budget;
    		}
    	}
    	return budget_total;
    }
    
    /*ETAPE 10*/
    static void afficherNomsFilmsRealisateur (Film[] lesFilms, String pnomRealisateur)
    {
    	
    	
    	for (int i = 0; i<lesFilms.length; i++)
    	{
    	 	if (lesFilms[i].nom_realisateur.equalsIgnoreCase(pnomRealisateur))
    	 	{
    	 		System.out.println ("Le film correspondant au nom de son r�alisateur est " + lesFilms[i] + "." + System.lineSeparator());
    		}
    	}
    
    }
    
    //ETAPE 11
    static void afficherNomsFilmsSuissesApresUneDate (Film[] lesFilms, int pdateFilm)
    {
    
    	
    	System.out.println ("Les films suisses r�alis�s apr�s une date donn�e sont : " + System.lineSeparator());
    	
    	for (int i = 0; i<lesFilms.length; i++)
    	{
    		if ((lesFilms[i].annee_film>=pdateFilm)&&(lesFilms[i].nationalite == true))
    		{
    			System.out.println (lesFilms[i]);
    		}
    	}
    }
    
    //ETAPE 12
    
    static void afficherNomsFilmsSuissesAvecBudgetSuperieur (Film[] lesFilms, int pbudgetFilm)
    {
    	Film premierFilm = lesFilms[0];
    	
  		System.out.println ();
    	System.out.println ("Les films suisses ayant un budget sup�rieur � une somme donn�e sont : " + System.lineSeparator());
    	
    	for (int i = 0; i<lesFilms.length; i++)
    	{
    		if ((lesFilms[i].budget > pbudgetFilm)&&(lesFilms[i].nationalite == true))
    		{
    			System.out.println (lesFilms[i].nom_film);
    		}
    	}
    }
    	
    //EXERCICE IMAGINAIRE
    
    	
    
    /*msteinle: Un boolean est une valeur qui ne peut prendre que la valeur vraie ou faux (false ou true en Java).
    static void exempleDeParametre(boolean unParam)
    {
    	if(unParam == true) // vous pouvez aussi �crire directement: if(unParam) { ...
    	{
    		System.out.println("C'est vrai!");
    	}
			else
			{
				System.out.println("C'est faux!");
			}
		}  */ 
}