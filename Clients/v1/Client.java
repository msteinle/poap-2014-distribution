/**
 * @(#)Client.java
 *
 *
 * @Artan Latifi
 * @version 1.00 2015/2/26
 */


public class Client {

	private String nom, prenom;
	private int numero;
	private double dette;

    public Client(String pNom, String pPrenom) {
    	
    	nom= pNom;
    	prenom= pPrenom;
    	
    	//nom= "ZeGrande";
    	//prenom="Isaac";
    }
    
    public String toString() {
    	String text = "Client :\t" + nom + " " + prenom;
    	return text;
    }
    
    
}