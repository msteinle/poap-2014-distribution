/**
 * @(#)TestClient.java
 *
 *
 * @Artan <3
 * @version 1.00 2015/2/26
 */

public class TestClient {
        
    /**
     * Creates a new instance of <code>TestClient</code>.
     */
    public TestClient() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Client client1 = new Client("ZeGrande", "Isaac");
        System.out.println (client1);
        
        Client client2 = new Client("Blaise", "MJ");
        System.out.println (client2);
    }
}
