/**
 * @(#)TestClient.java
 *
 *
 * @Artan <3
 * @version 1.00 2015/2/26
 */

public class TestClientAvecTableau {
 
	final static int NOMBRE_CLIENTS = 3; 
 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Client[] lesClients = new Client[NOMBRE_CLIENTS];
        
        lesClients[0] = new Client("ZeGrande", "Isaac");   
        lesClients[1] = new Client("Blaise", "MJ");       
        lesClients[2] = new Client("Doe", "John");
        
        for (int z = 0; z<lesClients.length; z++) {
        	System.out.println (lesClients[z]);
        }
        
    }
}
