import java.io.FileNotFoundException;
import java.lang.Exception;
import java.lang.String;
import java.lang.System;
import java.util.Scanner;
import java.io.File;

 /* ===== Exercice =====
Complétez le code fourni selon les instructions fournies. Testez à chaque étape!
1.  Créer une méthode calculerDetteMax qui calcule la dette maximale, puis complétez l'affichage.
2.  Faites de même pour la moyenne et le minimum.
3.  Ensuite, de la même manière, créez une méthode qui compte tous les clients qui ont des dettes et afficher ce nombre.
4.  Rajouter un attribut "vip" de type boolean à la Classe client.
5.  Compléter le constructeur pour accepter également un paramètre pVip et initialiser l'attribut vip.
6.  Compléter le toString() pour afficher ce nouvel attribut.
7.  Compléter le fichier ListeClients en ajoutant "true" respectivement "false" à chaque client.
8.  Adapter la lecture du fichier et la création des clients pour prendre en compte cette nouvelle valeur.
9.  Ajouter une méthode pour compter le nombre de clients VIP et l'afficher.
10. Modifier la méthode toString(), afin qu'elle retourne "VIP" à la place de "Client", et masque le nom si le client est un VIP.
    Par exemple, si Tyrion Lannister est un VIP, il faut afficher "VIP - Numéro : 7, Dette : 0.0" à la place de l'affichage précédent.
==== Fin instructions exercice === */
public class CalculsClients
{

    final static int NOMBRE_CLIENTS = 5;

    public static void main(String[] args) {


        Client[] lesClients = lireClients();

        afficherClients(lesClients);
        double somme = calculerDetteTotale(lesClients);

        System.out.println ("________________________________________________");
        System.out.println("Le total des dettes est de " + somme);

       

    }


    static Client[] lireClients()
    {
        Client[] clients = new Client[NOMBRE_CLIENTS];
        try {

            File fichier = new File("ListeClients.txt");
            Scanner sc = new Scanner(fichier);

            for(int i = 0; i < clients.length; i++) {

                //Lecture
                String nomCli = sc.next();
                String prenomCli = sc.next();
                int numCli = sc.nextInt();
                double detteCli = sc.nextDouble();

                //Création d'un nouveau client
                Client nouveauCli = new Client(nomCli, prenomCli, numCli, detteCli);

                //Mettre le client dans le tableau
                clients[i] = nouveauCli;

            }
        } catch(Exception ex) {
            // Afficher l'exception
            ex.printStackTrace();
        }
        return clients;
    }

    static void afficherClients(Client[] nosClients)
    {
        for (int i = 0; i< nosClients.length; i++){
            System.out.print ("\t" + i + ". ");
            System.out.println (nosClients[i]);
        }
    }

    static double calculerDetteTotale(Client[] lesClients)
    {
        double sommeDesDettes = 0;
        for (int j = 0; j < lesClients.length; j++)
        {
            // Récupérer le client courant
            Client leClient = lesClients[j];
            // Récupérer la dette du client courant
            double detteDuClient = leClient.dette;
            // Rajouter cette dette à la dette totale
            sommeDesDettes = sommeDesDettes + detteDuClient;
        }
        return sommeDesDettes;
    }

}