/**
 * @(#)Client.java
 *
 *
 * @author Artan Latifi
 * @version 1.00 2015/2/26
 */
public class Client {

	/* private */ String nom, prenom;
	/* private */ int numero;
	/* private */ double dette;
	/* private */ boolean vip;

    public Client(String pNom, String pPrenom, int pNumero, double pDette, boolean pVip) {
    	
    	nom = pNom;
    	prenom = pPrenom;
    	numero = pNumero;
    	dette = pDette;
    	vip = pVip;

    }
    
    public String toString() {
    	
    	String text;

    	if(vip == true)
    	{
    		text = "VIP :\t" + " Numéro : " + numero + " Dette : " + dette ;
    	}
    	else
    	{
    		text = "Client :\t" + nom + " " + prenom + " Numéro : " + numero + " Dette : " + dette + " N'est pas VIP ";
    	}

    	
    	/*String text = "Client :\t" + nom + " " + prenom + " Numéro : " + numero + " Dette : " + dette + " Vip : " + vip;*/
    	return text;
    }
    
    
}