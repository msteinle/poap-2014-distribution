/**
 * @(#)Client.java
 *
 *
 * @author Artan Latifi
 * @version 1.00 2015/2/26
 */
public class Client {

	/* private */ String nom, prenom;
	/* private */ int numero;
	/* private */ double dette;

    public Client(String pNom, String pPrenom, int pNumero, double pDette) {
    	
    	nom = pNom;
    	prenom = pPrenom;
    	numero = pNumero;
    	dette = pDette;

    }
    
    public String toString() {
    	String text = "Client :\t" + nom + " " + prenom + " Numéro : " + numero + " Dette : " + dette ;
    	return text;
    }
    
    
}