/**
 * @(#)Clients.java
 *
 *
 * @MJB, KN 
 * @version 1.00 2015/3/11
 */

import java.util.Scanner;
import java.io.File;


public class LireClient {
        
    final static int NOMBRE_CLIENTS = 5;

    public static void main(String[] args) {
    	Client[] tab = new Client[NOMBRE_CLIENTS];
    	
    	try {
    		
	    	File fichier = new File("ListeClients.txt");
			Scanner sc = new Scanner(fichier);
			
        	for(int i = 0; i < tab.length; i++) {
        		
        		//Lecture
        		String nomCli = sc.next();
        		String prenomCli = sc.next();
        		int numCli = sc.nextInt();
        		double detteCli = sc.nextDouble();
        		
        		//Cr�ation d'un nouveau client
        		Client nouveauCli = new Client(nomCli, prenomCli, numCli, detteCli);       		
        		
        		//Mettre le client dans le tableau		
	        	tab[i] = nouveauCli;	        		
	        	
        	} 	        		        
        } catch(Exception ex) {
	       	ex.printStackTrace();
	    }
        
        //Affichage du client
        for (int i = 0; i< tab.length; i++){
        	System.out.println (tab[i]);
        }//for
        
    }//main
}//class