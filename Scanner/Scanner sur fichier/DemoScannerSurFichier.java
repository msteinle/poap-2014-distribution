/**
 * @(#)DemoScannerSurFichier.java
 *
 *
 * @author 
 * @version 1.00 2015/3/5
 */
import java.util.Scanner;
import java.io.File;

public class DemoScannerSurFichier {
        
    /**
     * Creates a new instance of <code>DemoScannerSurFichier</code>.
     */
    public DemoScannerSurFichier() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    	try {
	    	File f = new File("DemoScannerSurFichier1.txt");
	        Scanner sc = new Scanner(f);
	        while(sc.hasNextInt()) {
		        System.out.println (sc.nextInt());
	        }
        } catch(Exception ex) {
        	System.out.println (ex);
        }
    }
}
