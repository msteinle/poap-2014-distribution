/**
 * @(#)RemplirTableauAvecScanner.java
 *
 *
 * @author 
 * @version 1.00 2015/3/6
 */
import java.util.Scanner;
import java.io.File;

public class RemplirTableauAvecScanner {
        
	final static int NOMBRE_ELEMENTS = 5;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // initialisation d'un tableau de longueur NOMBRE_ELEMENTS
        int[] tab = new int[NOMBRE_ELEMENTS];
        
        // PHASE I : remplir le tableau avec Scanner
        try {
    		
	    	File f = new File("DemoScannerSurFichier1.txt");
	        Scanner sc = new Scanner(f);
        
	        for(int i = 0; i < tab.length; i++) {
	        	tab[i] = sc.nextInt();
	        }
        } catch(Exception ex) {
	       	ex.printStackTrace();
	    }
	    
	    // PHASE II : calculer avec le tableau
	    for(int i = 0; i < tab.length; i++) {
	    	System.out.println (tab[i]);
	    }
    }
}
