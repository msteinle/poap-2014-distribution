import java.util.Scanner;

class DemoScanner
{
	public static void main (String[] args) 
	{
		String donn�esATraiter = "Bonjour les amis";
		
		exemple1(donn�esATraiter);//uniquement des cha�nes
		
		//exemple2(); // avec des "double"s
		
		//exemple3(); // boucle car nombre de donn�ees inconnu
		//exemple4();
	}
	
	private static void exemple1(String d)
	{
		Scanner flux = new Scanner(d);
		System.out.println (flux.next());// next() = la prochaine cha�ne
		System.out.println (flux.next());
		System.out.println (flux.next());
	//  une lecture de trop d�clenche l'exception NoSuchElement
	//	System.out.println (flux.next());
	
	//=======
	// TODO: Modifiez la valeur du param�tre pass� en entr�e, 
	// pour que la ligne ci-dessus ne d�clenche PAS d'exception.		
	}
	
	private static void exemple2()
	{
		Scanner flux = new Scanner("12.5 57");
		double base = flux.nextDouble();// si pas nombre : InputMismatchException
		double hauteur = flux.nextDouble();
		System.out.println (base*hauteur);
		
		//=======
		// TODO: Modifiez la cha�ne de caract�res / String pass�e au constructeur
		// de Scanner, pour que InputMismatchException soit d�clench�e.	
	}
	
	private static void exemple3()
	{
		Scanner flux = new Scanner("a b cd efg hj i o");
		
		int num = 1;
		while ( flux.hasNext() )// hasNext() = est-ce qu'il reste qq ch sur le flux
		{
			System.out.println (num + " : " + flux.next()); // si oui on le consomme
			num++; // �quivalent de num = num + 1
		}
	}
	
	private static void exemple4()
	{
		Scanner flux = new Scanner("a 2 cd 3 hj 4 o 5 k w yx");
		
	//	int num = 1;
		int somme = 0;
		while ( flux.hasNext() )// hasNext() = est-ce qu'il reste qq ch sur le flux
		{
			if ( flux.hasNextInt() )
				somme = somme + flux.nextInt();
			else
				flux.next(); // se contente de passer au suivant sans rien faire de ce qui est lu
		}
		System.out.println ("somme = " +  somme);
	}
	
	//=======
	// TODO: rajoutez un exemple qui sache faire la somme des nombres dans
	// la cha�ne ci-dessous, ainsi que la concat�nation des lettres:
	// "a 2.5 cd 3.34 hj 4 o 5 k w yx"
}